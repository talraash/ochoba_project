import asyncio
import emoji
from aiogram import Dispatcher, executor, Bot
from aiogram import types

from ochoba_project.tokens import telegram_token, dtf_token, telegram_id
from ochoba_project.api_part import user_updates, api, event_id, save_to_archive, tmp_id_from_csv, add_comment_request
from ochoba_project.api_part import vote_send, get_comments_tree, take_new_feed, get_entry


dp = Dispatcher(Bot(token=telegram_token))

answer_msg = ''
session_events_id = []
last_id = 0


async def votes(one_event, message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(
        text='К комментарию', url=one_event['url']))
    entry_id = one_event['url'].split('/')[-1].split('-')[0]
    comment_id = one_event['url'].split('/')[-1].split('=')[-1]
    voted_comments = get_comments_tree(
        api_version=api.v1, entry_id=entry_id, comment_id=comment_id)
    for one_comment in voted_comments['result']['items']:
        if str(comment_id) == str(one_comment['id']):
            comment_to_send = one_comment['text']
            if one_event['color'] == 'green':
                emote = emoji.emojize(':thumbs_up:')
            else:
                emote = emoji.emojize(':thumbs_down:')
            await asyncio.sleep(1)
            await message.answer(emote + f"{one_event['text'].split(' ')[0]} -  Оценил ваш комментарий: ")
            await message.answer(comment_to_send, reply_markup=keyboard)


async def comment(one_event, message: types.Message):
    base_id = one_event['url'].split('/')[-1].split('-')[0]
    rep_to_id = one_event['url'].split('/')[-1].split('=')[-1]
    buttons = [types.InlineKeyboardButton(text=emoji.emojize(":thumbs_up:"), callback_data=f'{rep_to_id}:just:like'),
               types.InlineKeyboardButton(
                   text='reset', callback_data=f'{rep_to_id}:just:reset'),
               types.InlineKeyboardButton(text=emoji.emojize(
                   ":thumbs_down:"), callback_data=f'{rep_to_id}:just:dislike'),
               types.InlineKeyboardButton(
                   text='Send answer', callback_data=f'{base_id}:{rep_to_id}')
               ]
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    keyboard.add(*buttons)
    await message.answer(one_event['comment_text'], reply_markup=keyboard)


def aut(func):

    async def checker(message: types.Message):
        if int(message['from']['id']) != telegram_id:
            return await message.reply('Access denied!', reply=False)
        return await func(message)

    return checker


@dp.message_handler(commands='comments')
@aut
async def give_me_comments(message: types.Message):
    tmp_upd = user_updates(dtf_token)
    for one_event in tmp_upd:
        if one_event['type'] == event_id.comment_answer:
            await comment(one_event=one_event, message=message)


@dp.message_handler(commands='votes')
@aut
async def give_me_votes(message: types.Message):
    tmp_upd = user_updates(dtf_token)
    for one_event in tmp_upd:
        if one_event['type'] == event_id.new_vote:
            await votes(one_event=one_event, message=message)


@dp.message_handler(commands=['start', 'stop'])
@aut
async def bot_upd(message: types.Message):
    global session_events_id
    if not bool(session_events_id):
        response = user_updates(dtf_token)
        for one_event in response:
            session_events_id.append(str(one_event['id']))
    if message.get_command() == '/start':
        await message.answer(text='/stop - остановить получение уведомление.')
        await message.answer(text='/new_feed - получение новых материалов из свежего.')
        await message.answer(text='/comments - получение последних комментариев.')
        await message.answer(text='/votes - получение последних оценок')
    while True:
        if message.get_command() == '/stop':
            break
        await asyncio.sleep(10)
        response = user_updates(dtf_token)
        for one_event in response:
            if str(one_event['id']) in session_events_id:
                continue
            else:
                session_events_id.append(str(one_event['id']))
                if one_event['id'] not in tmp_id_from_csv():
                    tmp_data = {'post_id': str(one_event['id']), 'event_type': str(one_event['type']),
                                'date': str(one_event['date']), 'dateRFC': str(one_event['dateRFC']),
                                'text': str(one_event['text']), 'url': str(one_event['url'])
                                }
                    save_to_archive(tmp_data)
                if one_event['type'] == event_id.new_vote:
                    await votes(one_event=one_event, message=message)
                elif one_event['type'] == event_id.comment_answer:
                    await comment(one_event=one_event, message=message)


@dp.message_handler(commands='new_feed')
@aut
async def new_feed(message: types.Message):
    global last_id, answer_msg
    response_code, last_five = take_new_feed()
    if str(response_code) == '200':
        for item in last_five:
            if last_id < item['data']['id']:
                entry = get_entry(api_version=api.v1,
                                  entry_id=item['data']['id']).json()['result']
                buttons = [types.InlineKeyboardButton(text='К статье', url=entry['url']),
                           types.InlineKeyboardButton(
                               text='Отправить комментарий', callback_data=f'{item["data"]["id"]}:0')
                           ]
                keyboard = types.InlineKeyboardMarkup(row_width=2)
                keyboard.add(*buttons)
                last_id = item['data']['id']
                try:
                    text_msg = entry['intro'][:80]
                except KeyError:
                    text_msg = entry['summarize'][:80]
                if text_msg:
                    await message.answer(text_msg, reply_markup=keyboard)
                else:
                    await message.answer('Пост без текста?', reply_markup=keyboard)
    else:
        await message.answer(f'Что-то не так, самое время прикрутить логи и покопаться в них.Ответ сервера: '
                             f'{response_code}')


@dp.message_handler()
@aut
async def take_message(message: types.Message):
    global answer_msg
    answer_msg = message.text


@dp.callback_query_handler()
async def answer(call: types.CallbackQuery):
    global answer_msg
    ids = call['data'].split(':')
    if len(ids) == 2:
        add_comment = add_comment_request(api_version=api.v1, token=dtf_token, content_id=ids[0],
                                          answer_message=answer_msg, reply_to_id=ids[-1]
                                          )
        if str(add_comment.status_code) == '200':
            await call.message.answer(text=f'Ваш комментарий отправлен: {answer_msg[:80]}')
            await call.answer()
        else:
            await call.message.answer(text=f'Что-то не так, самое время прикрутить логи и покопаться в них. '
                                           f'Ответ сервера: {add_comment.status_code}'
                                      )
            await call.answer()
    elif len(ids) == 3:
        sign = {'like': 1, 'dislike': -1, 'reset': 0}
        create_vote = vote_send(
            api_version=api.v1, token=dtf_token, comment_id=ids[0], sing_id=sign[ids[-1]])
        if str(create_vote.status_code) == '200':
            if sign[ids[-1]] == 0:
                await call.message.answer(text='Вы обнулили свою оценку комментарию')
                await call.answer()
            elif sign[ids[-1]] == 1:
                await call.message.answer(text='Вы повысили на 1 оценку комментарию')
                await call.answer()
            else:
                await call.message.answer(text='Вы понизили на 1 оценку комментарию')
                await call.answer()
        else:
            await call.message.answer(text=f'Что-то не так, самое время прикрутить логи и покопаться в них. '
                                           f'Ответ сервера: {create_vote.status_code}'
                                      )
            await call.answer()


def main_func() -> None:
    executor.start_polling(dp, skip_updates=True, relax=10)
