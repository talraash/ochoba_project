import requests
import csv
from os.path import exists
from collections import namedtuple


"""
QUERY PARAMETERS

allSite type: boolean True: Показать ленту всего сайта. 
False: Показать ленту только указанных подсайтов в subsitesIds или личную ленту пользователя

sorting type:str {hotness: по популярности,
                  date: свежее,
                  day: за день,
                  week: за неделю,
                  month: месяц,
                  year: за год,
                  all: за все время
                  }

subsitesIds type:str Id подсайтов через запятую, которые нужно включить в ленту

hashtag type:str Показать записи только с указанным хештегом

lastId  type:int Значение lastId из предыдущего запроса

lastSortingValue type:int Значение lastSortingValue из предыдущего запроса
"""

Api = namedtuple('Api', ['v1', 'v2', 'v3'])
Sorting = namedtuple(
    'Sorting', ['hotness', 'date', 'day', 'week', 'month', 'year', 'all'])
Event_ids = namedtuple("Event", ['new_vote', 'comment_answer'])

api = Api('v1.9.0', 'v2.1.0', 'v2.31')
sorting = Sorting('hotness', 'date', 'day', 'week', 'month', 'year', 'all')
event_id = Event_ids(2, 4)
allSite = True
answer_msg = ''
session_events_id = []


def save_to_archive(data_to_save):
    """Save data from server response to csv file"""
    fieldnames = ['post_id', 'event_type', 'date', 'dateRFC', 'text', 'url']
    if exists('updates_archive.csv'):
        with open('updates_archive.csv', 'a', encoding='UTF8') as file_to_save:
            writer = csv.DictWriter(file_to_save, fieldnames=fieldnames)
            writer.writerow(data_to_save)
    else:
        with open('updates_archive.csv', 'a', encoding='UTF8') as file_to_save:
            writer = csv.DictWriter(file_to_save, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow(data_to_save)


def tmp_id_from_csv():
    """Read from archive list of events"""
    tmp_id_list = []
    if exists('updates_archive.csv'):
        with open('updates_archive.csv', 'r') as file_to_read:
            for one_id in csv.DictReader(file_to_read):
                tmp_id_list.append(one_id['post_id'])
    return tmp_id_list


def user_updates(token):
    updates = requests.get(
        url=f'https://api.dtf.ru/{api.v1}/user/me/updates', headers={'X-Device-Token': token})
    if updates.status_code != 200:
        print('Somthing wrong with server response')
    tmp_response = updates.json()['result']
    return tmp_response


def take_new_feed():
    new_feed = requests.get(url=f'https://api.dtf.ru/v2.31/feed?pageName=new&sorting=all', headers={'charset': 'utf-8'})
    response_code = new_feed.status_code
    last_five = new_feed.json()['result']['items'][:5][::-1]
    return response_code, last_five


def add_comment_request(api_version, token, content_id, answer_message, reply_to_id):
    add_comment = requests.post(url=f'https://api.dtf.ru/{api_version}/comment/add', headers={'X-Device-Token': token},
                                files={'id': (None, content_id), 'text': (None, answer_message), 'reply_to': (None, reply_to_id)})
    return add_comment


def vote_send(api_version, token, comment_id, sing_id):
    create_vote = requests.post(url=f'https://api.dtf.ru/{api_version}/like', headers={'X-Device-Token': token},
                                files={'id': (None, comment_id), 'type': (None, 'comment'), 'sign': (None, sing_id)}
                                )
    return create_vote


def get_comments_tree(api_version, entry_id, comment_id):
    voted_comments = requests.get(url=f'https://api.dtf.ru/{api_version}/entry/{entry_id}/comments/thread/{comment_id}').json()
    return voted_comments


def get_entry(api_version, entry_id):
    entry = requests.get(url=f'https://api.dtf.ru/{api_version}/entry/{entry_id}')
    return entry
